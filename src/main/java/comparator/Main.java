package comparator;

import jdk.internal.org.xml.sax.SAXException;
import org.w3c.dom.Node;
import org.w3c.dom.Text;

import javax.lang.model.element.Element;
import javax.xml.parsers.*;
import java.io.IOException;

public class Main {
    public static void main (String[] args)throws ParserConfigurationException, IOException, SAXException{
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        DocumentBuilder builder = factory.newDocumentBuilder();
        org.w3c.dom.Document document;
        document = builder.newDocument();
        Element root = (Element) (Element) document.createElement("root");
        Element font = (Element) (Element) document.createElement("font");
        Text text = document.createTextNode("text");
        document.appendChild((Node) font);
        document.appendChild(text);
    }
}
