package controller;


import java.util.logging.LogManager;
import java.util.logging.Logger;


public interface Controller {
    Logger logger = LogManager.getLogger(Controller.class);
    void execute();
}
