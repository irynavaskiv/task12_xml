<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
    <xsl:template match="/">
        <html>

            <body>
                <table class="tfmt">
                    <tr>
                        <th style="width:250px">Name</th>
                        <th style="width:350px">Solid</th>
                        <th style="width:250px">Origin</th>
                        <th style="width:250px">Colorofthestem</th>
                        <th style="width:250px">Thecoloroftheleaves</th>
                        <th style="width:250px">Averageplantsize</th>
                        <th style="width:250px">Temperature</th>
                        <th style="width:250px">Lighting</th>
                        <th style="width:250px">Watering</th>
                        <th style="width:250px">Multiplying</th>
                    </tr>

                    <xsl:for-each select="gems/gem">

                        <tr>
                            <td class="colfmt">
                                <xsl:value-of select="@fl"/>
                            </td>

                            <td class="colfmt">
                                <xsl:value-of select="name" />
                            </td>
                            <td class="colfmt">
                                <xsl:value-of select="solid" />
                            </td>

                            <td class="colfmt">
                                <xsl:value-of select="origin" />
                            </td>
                            <td class="colfmt">
                                <xsl:value-of select="colorofthestem" />
                            </td>

                            <td class="colfmt">
                                <xsl:value-of select="averageplantsize" />
                            </td>

                            <td class="colfmt">
                                <xsl:value-of select="temperature" />
                            </td>

                            <td class="colfmt">
                                <xsl:value-of select="lighting" />
                            </td>

                            <td class="colfmt">
                                <xsl:value-of select="watering" />
                            </td>

                            <td class="colfmt">
                                <xsl:value-of select="multiplying" />
                            </td>

                        </tr>

                    </xsl:for-each>
                </table>
            </body>

            <head>
                <style type="text/css">
                    table.tfmt {
                    border: 3px ;
                    }
                    td.colfmt {
                    border: 1px ;
                    background-color: grey;
                    color: red;
                    text-align:right;
                    }
                    th {
                    background-color: #2E9AFE;
                    color: white;
                    }

                </style>
            </head>

        </html>
    </xsl:template>

</xsl:stylesheet>